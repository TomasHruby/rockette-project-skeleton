SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `account` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(127) NULL,
	`midname` VARCHAR(127) NULL,
	`surname` VARCHAR(127) NULL,
	`username` VARCHAR(63) NOT NULL,
	`email` VARCHAR(255) NOT NULL COMMENT 'Used for login',
	`phone` VARCHAR(31) NULL,
	`password` VARCHAR(255) NULL COMMENT 'Used for login',
	`status` TINYINT(4) NOT NULL,
	`metadata` JSON NULL,
	`timezone` VARCHAR(63) NULL,
	`country` VARCHAR(2) NULL,
	`language` VARCHAR(2) NULL,
	`create_date` DATETIME NULL,
	`email_verify_date` DATETIME NULL,
	`phone_verify_date` DATETIME NULL,
	`login_date` DATETIME NULL,
	UNIQUE KEY `username` (`username`),
	UNIQUE KEY `email` (`email`),
	PRIMARY KEY (`id`)
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;

CREATE TABLE `account_to_organization` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`account_id` INT(10) UNSIGNED NOT NULL,
	`organization_id` INT(10) UNSIGNED NOT NULL,
	`role` TINYINT(4) NOT NULL,
	`create_date` DATETIME NULL,
	UNIQUE KEY `account_id_organization_id` (`account_id`, `organization_id`),
	PRIMARY KEY (`id`),
	KEY `account_to_organization_fk_organization_id` (`organization_id`),
	CONSTRAINT `account_to_organization_fk_account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT `account_to_organization_fk_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;
