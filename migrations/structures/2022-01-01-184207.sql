SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `country`
ADD COLUMN `in_schengen` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Is country in Schengen economic zone?' AFTER `in_eu`;
