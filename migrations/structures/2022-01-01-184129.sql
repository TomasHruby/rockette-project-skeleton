SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `country`
MODIFY COLUMN `phone_prefix` VARCHAR(15) NOT NULL AFTER `in_eu`,
DROP COLUMN `in_schengen`;
