SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `organization`
MODIFY COLUMN `contact_email` VARCHAR(128) NULL AFTER `slug`,
MODIFY COLUMN `contact_phone` VARCHAR(32) NULL AFTER `contact_email`;

CREATE TABLE `user_to_organization` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(10) UNSIGNED NOT NULL,
	`organization_id` INT(10) UNSIGNED NOT NULL,
	`role` TINYINT(4) NOT NULL,
	`create_date` DATETIME NULL,
	UNIQUE KEY `user_id_organization_id` (`user_id`, `organization_id`),
	PRIMARY KEY (`id`),
	KEY `user_to_organization_fk_organization_id` (`organization_id`),
	CONSTRAINT `user_to_organization_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	CONSTRAINT `user_to_organization_fk_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
)
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci
ENGINE=InnoDB;
