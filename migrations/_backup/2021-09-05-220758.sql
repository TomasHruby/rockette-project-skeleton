SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `user`
ADD COLUMN `username` VARCHAR(32) NOT NULL AFTER `surname`,
ADD COLUMN `access` INT(11) NOT NULL AFTER `password`,
ADD COLUMN `login_date` DATETIME NULL AFTER `verify_date`,
ADD UNIQUE KEY `username` (`username`),
MODIFY COLUMN `firstname` VARCHAR(64) NULL AFTER `id`,
MODIFY COLUMN `surname` VARCHAR(64) NULL AFTER `firstname`,
MODIFY COLUMN `email` VARCHAR(128) NOT NULL COMMENT 'Used for login' AFTER `username`,
MODIFY COLUMN `phone` VARCHAR(32) NULL AFTER `email`,
MODIFY COLUMN `create_date` DATETIME NULL AFTER `access`,
DROP COLUMN `nickname`,
DROP COLUMN `is_active`;
