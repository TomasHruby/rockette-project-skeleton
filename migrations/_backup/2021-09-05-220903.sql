SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `user`
MODIFY COLUMN `access` TINYINT(4) NOT NULL AFTER `password`;
