SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `user`
ADD COLUMN `metadata` JSON NULL AFTER `access`;
