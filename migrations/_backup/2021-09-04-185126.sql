SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

CREATE TABLE `organization` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL COMMENT 'Name or firstname with surname for small business',
	`secretary_firstname` VARCHAR(32) NULL,
	`secretary_surname` VARCHAR(32) NULL,
	`reg_number` VARCHAR(16) NOT NULL,
	`vat_number` VARCHAR(32) NULL,
	`website` VARCHAR(128) NULL,
	`contact_email` VARCHAR(128) NOT NULL,
	`contact_phone` VARCHAR(32) NOT NULL,
	`licence` INT(11) NULL,
	`create_date` DATETIME NULL,
	`is_blocked` TINYINT(1) UNSIGNED NOT NULL,
	`is_active` TINYINT(1) UNSIGNED NOT NULL,
	`column_test` TINYINT(1) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE `product` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	`sku` VARCHAR(32) NOT NULL,
	`identifier` VARCHAR(32) NULL,
	`ean` VARCHAR(32) NULL,
	`price` DECIMAL(15, 4) NOT NULL,
	`price_supplier` DECIMAL(15, 4) NULL,
	`price_original` DECIMAL(15, 4) NULL,
	`price_vat` DECIMAL(15, 4) NULL,
	`description` TEXT NULL,
	`create_date` DATETIME NULL,
	`is_active` TINYINT(1) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE `user` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(64) NOT NULL,
	`surname` VARCHAR(64) NOT NULL,
	`nickname` VARCHAR(32) NOT NULL,
	`email` VARCHAR(128) NOT NULL COMMENT 'Used for login',
	`phone` VARCHAR(32) NOT NULL,
	`password` VARCHAR(256) NULL COMMENT 'Used for login',
	`is_active` TINYINT(1) UNSIGNED NOT NULL,
	`create_date` DATETIME NULL,
	`verify_date` DATETIME NULL,
	UNIQUE KEY `email` (`email`),
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
CHARACTER SET=utf8mb4
COLLATE=utf8mb4_general_ci;
