SET foreign_key_checks = 1;
SET time_zone = "SYSTEM";
SET sql_mode = "TRADITIONAL";

ALTER TABLE `organization`
ADD COLUMN `free` INT(11) NULL AFTER `licence`;

ALTER TABLE `product`
ADD COLUMN `short_description` TEXT NULL AFTER `description`;
