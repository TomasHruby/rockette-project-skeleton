<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Repo;

use Model\Entity\Organization;

/**
 * @method Organization getSingle($id)
 * @method Organization[] getMultiple
 */
class OrganizationRepo extends BaseRepo
{

}
