<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Repo;

use Model\Entity\Address;

/**
 * @method Address getSingle($id)
 * @method Address[] getMultiple
 */
class AddressRepo extends BaseRepo
{

}
