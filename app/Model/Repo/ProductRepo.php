<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Model\Repo;

use Model\Entity\Product;

/**
 * @method Product getSingle($id)
 * @method Product[] getMultiple
 */
class ProductRepo extends BaseRepo
{

}
