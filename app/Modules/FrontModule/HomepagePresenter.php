<?php

declare(strict_types=1);

namespace Modules\FrontModule;

use Model\Repo\OrganizationRepo;

final class HomepagePresenter extends BasePresenter
{

    /**
     * @var OrganizationRepo @inject
     */
    public OrganizationRepo $organizationRepo;

    protected function startup() {
        parent::startup();

        $this->getUser()->getStorage()->setNamespace('frontend');
    }

    public function actionDefault() {

        \Tracy\Debugger::barDump($this->getUser()->getIdentity());

        //\Tracy\Debugger::barDump($this->userFacade->passwordEncode('test'));

        /*
        $user = $this->userRepo->getSingle(1);
        foreach($user->userOrganizations as $userOrganization){
            \Tracy\Debugger::barDump($user->firstname . ' / ' . $userOrganization->getRoleName() . ' / ' . $userOrganization->organization->name);
            //\Tracy\Debugger::barDump($user->firstname . ' ' . \Model\Enum\Role::$roleNames[$userOrganization->role] . ' / ' . $userOrganization->organization->name);
        }

        $org = $this->organizationRepo->getSingle(1);
        foreach($org->organizationUsers as $organizationUsers){
            \Tracy\Debugger::barDump($org->name . ' / ' . $organizationUsers->getRoleName() . ' / ' . $organizationUsers->user->firstname);
        }
        */

    }

}
