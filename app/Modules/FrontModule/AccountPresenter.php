<?php

declare(strict_types=1);

namespace Modules\FrontModule;

final class AccountPresenter extends SecuredPresenter
{

    public function actionDefault() {
        \Tracy\Debugger::barDump($this->getUser());

    }

    public function renderDefault() {

    }

}
