<?php

declare(strict_types=1);

namespace App\Router;

use Nette\Application\Routers\RouteList;

final class RouterFactory extends \Rockette\Router\RouterFactory
{

    public static function createRouter(): RouteList {

        $router = new RouteList;
        $router[] = self::getDevRouteList();
        $router[] = self::getAdminRouteList();
        $router[] = self::getFrontRouteList();
        return $router;
    }

    protected static function getFrontRouteList() {
        $router = new RouteList('Front');
        $router->addRoute('<presenter>/<action>[/<id>]', 'Homepage:default');
        return $router;
    }

    protected static function getAdminRouteList() {
        $router = new RouteList('Admin');
        $router->addRoute('admin', [ 'presenter' => 'Dashboard', 'action' => 'default']);
        $router->addRoute('admin/<presenter>[/<action>][/<id>][/<par1>]', [    'presenter' => 'Dashboard', 'action' => 'default' ]);
        return $router;
    }

}
