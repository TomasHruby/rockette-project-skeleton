<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Console;

final class CleanCommand extends \Rockette\Console\CleanCommand
{

    protected static $defaultName = 'clean';

}
