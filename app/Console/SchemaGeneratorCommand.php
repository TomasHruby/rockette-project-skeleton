<?php
/*
 * @author Tom Hruby
 * https://tomashruby.com
 */

namespace Console;

final class SchemaGeneratorCommand extends \Rockette\Console\SchemaGeneratorCommand
{

    protected static $defaultName = 'schema-generator';

}
